<?php

	//Model-unspecific methods
	class Allgemein
	{


		//
		//  Convert date from 'Y-m-d H:i:s' to 'H:i'
		//
		public static function doTimeFormat($date)
		{
			return date("H.i", strtotime($date)) . ' Uhr';
		}



		//
		//  Convert date from 'Y-m-d H:i:s' to 'd.m.Y'
		//
		public static function doDateFormat($date)
		{
			return date("d.m.Y", strtotime($date));
		}



		//
		//  Builds Menu-HTML-String for BorderMenu
		//
		public static function getBorderMenu()
		{
			$menu = '';
			$idUser = Yii::app()->user->id;
			$critMessage = new CDbCriteria();
			$critMessage->addCondition("touser = '$idUser' or fromuser = '$idUser'");
			$critMessage->order = 'date DESC';
			$mMessages = Message::model()->findAll($critMessage);
			if (!empty($mMessages)) {
				for ($i = 0; $i <= 4; $i++) {
					foreach ($mMessages as $message) {
						if ($message->fromuser == $idUser) {
							if (!strpos($menu, User::getUsername($message->touser))) {
								$menu .= '<li class="buzz-out"><a href="' . Yii::app()->createUrl('message/view', array('idFromUser' => $message->touser)) . '">' . User::getUsername($message->touser) . '</a></li>';
							}
						} else {
							if (!strpos($menu, User::getUsername($message->fromuser))) {
								$countUnread = Readstatus::getReadstatusChat($message->fromuser);
								if ($countUnread != 0) {
									$menu .= '<li class="buzz-out"><a href="' . Yii::app()->createUrl('message/view', array('idFromUser' => $message->fromuser)) . '"><span class="label label-info">' . $countUnread . '</span> &nbsp;' . User::getUsername($message->fromuser) . '</a></li>';
								} else {
									$menu .= '<li class="buzz-out"><a href="' . Yii::app()->createUrl('message/view', array('idFromUser' => $message->fromuser)) . '">' . User::getUsername($message->fromuser) . '</a></li>';
								}
							}
						}
					}
				}
			}
			$menu .= '<li class="buzz-out"><a href="' . Yii::app()->createUrl('message/new') . '">New Chat</a></li>';
			$menu .= '<li class="buzz-out"><a href="' . Yii::app()->createUrl('site/about') . '" >About </a></li>';
			$menu .= '<li class="buzz-out"><a href="' . Yii::app()->createUrl('user/view') . '">Profil</a></li>';
			if ($idUser == 1) {
				$menu .= '<li class="buzz-out"><a href="' . Yii::app()->createUrl('user/admin') . '">Admin</a></li>';
			}
			$menu .= '<li class="buzz-out"><a href="' . Yii::app()->createUrl('site/logout') . '">Logout</a></li>';

			return $menu;
		}



		//
		//  Builds Menu-HTML-String for googleNexusWebsiteMenu
		//
		public static function getMenu()
		{
			$menu = '<li class="gn-search-item">
						<input placeholder="Suchen" type="search" class="gn-search">
						<a class="gn-icon fa-search"><span>Suchen</span></a>
					</li>';
			$idUser = Yii::app()->user->id;
			$critMessage = new CDbCriteria();
			$critMessage->addCondition("touser = '$idUser' or fromuser = '$idUser'");
			$critMessage->order = 'date DESC';
			$mMessages = Message::model()->findAll($critMessage);
			if (!empty($mMessages)) {
				$menu .= '<li><a href="' . Yii::app()->createUrl('message/view') . '" class="gn-icon fa-comments">Messages</a> <ul class="gn-submenu">';
				for ($i = 0; $i <= 4; $i++) {
					foreach ($mMessages as $message) {
						if ($message->fromuser == $idUser) {
							if (!strpos($menu, User::getUsername($message->touser))) {
								$menu .= '<li><a href="' . Yii::app()->createUrl('message/view', array('idFromUser' => $message->touser)) . '">' . User::getUsername($message->touser) . '</a></li>';
							}
						} else {
							if (!strpos($menu, User::getUsername($message->fromuser))) {
								$countUnread = Readstatus::getReadstatusChat($message->fromuser);
								if ($countUnread != 0) {
									$menu .= '<li><a href="' . Yii::app()->createUrl('message/view', array('idFromUser' => $message->fromuser)) . '">' . User::getUsername($message->fromuser) . '&nbsp <span class="label label-info">' . $countUnread . '</span> </a></li>';
								} else {
									$menu .= '<li><a href="' . Yii::app()->createUrl('message/view', array('idFromUser' => $message->fromuser)) . '">' . User::getUsername($message->fromuser) . '</a></li>';
								}
							}
						}
					}
				}
				$menu .= '</ul></li>';
			}
			$menu .= '<li><a href="' . Yii::app()->createUrl('message/new') . '" class="gn-icon fa-plus">New Chat</a></li>';
			$menu .= '<li><a href="' . Yii::app()->createUrl('site/about') . '" class="gn-icon fa-info">About </a></li>';
			$menu .= '<li><a href="' . Yii::app()->createUrl('user/view') . '" class="gn-icon fa-user">Profil</a></li>';
			if ($idUser == 1) {
				$menu .= '<li><a href="' . Yii::app()->createUrl('user/admin') . '" class="gn-icon fa-cogs">Admin</a></li>';
			}

			return $menu;
		}





		//
		//  LOADS THE UNREAD COUNT OF CURRENT URL
		//
		public static function getCountUnread()
		{

			if (Yii::app()->controller->getId() == 'message' && Yii::app()->controller->action->getId() == 'view') {
				$idFromUser = Yii::app()->getRequest()->getParam('idFromUser');
				if (!empty($idFromUser)) {
					return Readstatus::getReadstatusChat($idFromUser);
				} else {
					$idUser = Yii::app()->user->id;
					$critMessage = new CDbCriteria();
					$critMessage->addCondition("touser = '$idUser' or fromuser = '$idUser'");
					$critMessage->order = 'date DESC';
					$critMessage->limit = 1;
					$mMessage = Message::model()->find($critMessage);

					return Readstatus::getReadstatusChat($mMessage->fromuser);
				}
			}

			return 0;
		}
	}
