<?php

	class Readstatus extends CActiveRecord
	{

		public static function model($className = __CLASS__)
		{
			return parent::model($className);
		}



		public function tableName()
		{
			return 'sy_readstatus';
		}



		public function rules()
		{
			return array(
				array(
					'message, user, date',
					'required'
				),
				array(
					'message, user',
					'numerical',
					'integerOnly' => true
				),
				array(
					'id, message, user, date',
					'safe'
				),
				array(
					'id, message, user, date',
					'safe',
					'on' => 'search'
				),
			);
		}



		public function relations()
		{
			return array(
				'relUser' => array(
					self::BELONGS_TO,
					'User',
					'user'
				),
			);
		}



		public function attributeLabels()
		{
			return array(
				'id'      => 'ID',
				'message' => 'Nachricht',
				'user'    => 'Gelesen von',
				'date'    => 'Um',
			);
		}




		//
		//  gets read-status for a message timeline and returns the unread count --> called for menu-building
		//
		public static function getReadstatusChat($idFromuser)
		{
			$countUnread = 0;
			$idTouser = Yii::app()->user->id;
			$critMessage = new CDbCriteria();
			$critMessage->addCondition("fromuser = '$idFromuser'");
			$critMessage->addCondition("touser = '$idTouser'");
			$critMessage->order = 't.date DESC';
			$mMessages = Message::model()->findAll($critMessage);
			if (!empty($mMessages)) {
				foreach ($mMessages as $message) {
					//var_dump(' id: ' . $message->id);
					$idMessage = $message->id;
					$critReadstatus = new CDbCriteria();
					$critReadstatus->addCondition("message = '$idMessage'");
					$critReadstatus->addCondition("user = '$idTouser'");
					$mReadstatus = Readstatus::model()->find($critReadstatus);
					//var_dump('model: ' . $mReadstatus);
					if (is_null($mReadstatus)) {
						//var_dump(' read status empty ');
						$countUnread++;
					}
				}
			}
			//var_dump(' count unread: ' . $countUnread);
			//exit();
			return $countUnread;
		}



		//
		//  sets read-status for every message of message-timeline unread --> called on load of message with specified user
		//
		public static function setReadstatusChat($idFromuser)
		{
			$idTouser = Yii::app()->user->id;
			$critMessage = new CDbCriteria();
			$critMessage->addCondition("fromuser = '$idFromuser'");
			$critMessage->addCondition("touser = '$idTouser'");
			$mMessages = Message::model()->findAll($critMessage);
			foreach ($mMessages as $message) {
				self::setReadstatusMsg($message->id);
			}
		}



		//
		//  sets read-status for one message by user if previously unread
		//
		private static function setReadstatusMsg($idMessage)
		{
			$idTouser = Yii::app()->user->id;
			$critReadstatus = new CDbCriteria();
			$critReadstatus->addCondition("message = '$idMessage'");
			$critReadstatus->addCondition("user = '$idTouser'");
			$mReadstatus = Readstatus::model()->find($critReadstatus);
			if (empty($mReadstatus)) {
				$datetime = new \DateTime();
				$mReadstatus = new Readstatus();
				$mReadstatus->message = $idMessage;
				$mReadstatus->user = Yii::app()->user->id;
				$mReadstatus->date = $datetime->format('Y-m-d H:i:s');
				$mReadstatus->save();
			}
		}



		//
		//  returns read-status for one message
		//
		public static function getReadstatusMsg($idMessage)
		{
			$status = "";
			$critReadstatus = new CDbCriteria();
			$critReadstatus->addCondition("message = '$idMessage'");
			$mReadstatus = Readstatus::model()->findAll($critReadstatus);
			if (!empty($mReadstatus)) {
				foreach ($mReadstatus as $readstatus) {
					$status .= 'Gelesen von ' . User::getUsername($readstatus->user) . ' am ' . Allgemein::doDateFormat($readstatus->date) . " um " . Allgemein::doTimeFormat($readstatus->date) . '<br>';
				}
			} else {
				$status = 'Nicht gelesen';
			}

			return $status;
		}
	}