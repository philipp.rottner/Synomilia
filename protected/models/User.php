<?php


	class User extends CActiveRecord
	{

		public $password_repeat;



		public static function model($className = __CLASS__)
		{
			return parent::model($className);
		}



		public function tableName()
		{
			return 'sy_user';
		}



		public function rules()
		{
			return array(
				array(
					'name, password, password_repeat',
					'required'
				),
				array(
					'password',
					'compare'
				),
				array(
					'password',
					'length',
					'min' => 6,
					'max' => 45
				),
				array(
					'name',
					'unique'
				),
				array(
					'name, password',
					'length',
					'max' => 45
				),
				array(
					'picture',
					'length',
					'max' => 100
				),
				array(
					'id, name, password, password_repeat, picture',
					'safe',
					'on' => 'search'
				),
				array(
					'password_repeat, picture',
					'safe'
				),
			);
		}



		public function relations()
		{
			return array(
				'relMessage' => array(
					self::HAS_MANY,
					'Message',
					'user'
				),
			);
		}



		public function attributeLabels()
		{
			return array(
				'id'              => 'ID',
				'name'            => 'Name',
				'password'        => 'Passwort',
				'password_repeat' => 'Passwort-Wiederholung',
				'lastLoginTime'   => 'Letzter Login',
				'picture'         => 'Profilbild',
			);
		}



		public function search()
		{
			$criteria = new CDbCriteria;
			$criteria->compare('id', $this->id);
			$criteria->compare('name', $this->name, true);
			$criteria->compare('password', $this->password, true);
			$criteria->compare('lastLoginTime', $this->lastLoginTime, true);
			$criteria->compare('picture', $this->picture, true);

			return new CActiveDataProvider($this, array(
				'criteria'   => $criteria,
				'pagination' => false,
			));
		}



		protected function afterValidate()
		{
			parent::afterValidate();
			$this->password = $this->encrypt($this->password);
		}



		public function encrypt($value)
		{
			return md5($value);
		}



		public static function setLastLoginTime($idUser)
		{
			$model = UserController::loadModel($idUser);
			$date = new \DateTime();
			$model->lastLoginTime = $date->format('Y-m-d H:i:s');
			$model->save();
		}



		//
		//  Returns path to profile picture
		//
		public static function loadPictureUrl($filename)
		{
			if ($filename != "") {
				return '/pictures/' . $filename;
			}

			return 'http://placekitten.com/g/300/300';
		}


		//
		//  Counts total count of words of user
		//
		public static function getWordCount($idUser)
		{
			$totalWordCount = 0;
			$critMessage = new CDbCriteria();
			$critMessage->addCondition("fromuser = '$idUser'");
			$mMessages = Message::model()->findAll($critMessage);
			foreach ($mMessages as $message) {
				$totalWordCount += Message::getWordCount($message->text);
			}

			return $totalWordCount . " Wörter";
		}



		//
		//  Returns user name for cases where relation-based lazy-loading may fail
		//
		public static function getUsername($idUser)
		{
			$name = 'Unbekannt';
			$mUser = User::model()->findByPk($idUser);
			if (!empty($mUser)) {
				$name = $mUser->name;
			}

			return $name;
		}



		/*
		 * Returns when to expect answer statistically based on the avg. time from msgs. to the user and his response added onto the last login time or last msg., whichever was last
		 */
		public static function getEstimatedAnswerTime($idUser)
		{
			$responseTimeKumulated = 0;
			//Fetch all messages ever sent to user
			$critMessageToUser = new CDbCriteria();
			$critMessageToUser->addCondition("touser = '$idUser'");
			$critMessageToUser->order = 'date DESC';
			//$critMessage->group = 'idFromUser';
			$mMessagesToUser = Message::model()->findAll($critMessageToUser);
			$numberOfMsg = count($mMessagesToUser);
			//iterate through every message to find answer and add the response time to the kumulated one
			foreach ($mMessagesToUser as $msg) {
				$idMsg = $msg->id;
				$idFromUserMsg = $msg->idFromUser;
				$datetimeMsg = strtotime($msg->date);
				//get next answer from $idUser and calculate the time delta from toMsg to aw.
				$critAnswer = new CDbCriteria;
				$critAnswer->addCondition("idFromUser = '$idUser'");
				$critAnswer->addCondition("idToUser = '$idFromUserMsg'");
				$critAnswer->addCondition("id > '$idMsg'");
				$critAnswer->order = 'id ASC';
				$mAnswer = Message::model()->find($critAnswer);
				$datetimeAnswer = strotime($mAnswer->date);
				$responseTimeKumulated += $datetimeAnswer - $datetimeMsg;
			}
			//if user has receives messages:
			if ($numberOfMsg != 0) {
				//use static kumulated count of the time delta and the count of msgs to get the avg. response time
				$responseTimeAvg = $responseTimeKumulated / $numberOfMsg;
				//add this avg. to the last login time or last sent message, whichever was last
				$lastLoginTime = null;
				$lastMsgSent = mull;
				$mUser = User::model()->findByPk($idUser);
				//user has logged in
				if (!empty($mUser)) {
					$lastLoginTime = $mUser->lastLoginTime;
				}
				$critLastMsgSent = new CDbCriteria;
				$critLastMsgSent->addCondition("fromUser = '$idUser'");
				$critLastMsgSent->order = 'date DESC';
				$mLastMsgSent = Message::model()->find($critLastMsgSent);
				//user has sent an message
				if (!empty($mLastMsgSent)) {
					$lastMsgSent = $mLastMsgSent->date;
				}
				//last action plus the avg response time is the expected answer time if user receives a msg this very moment
				if ($lastLoginTime != null || $lastMsgSent != null) {
					//find last action (log-in or sent msg)
					$responseDatetimeExpected = null;
					$lastAction = null;
					if ($lastMsgSent > $lastLoginTime) {
						$lastAction = $lastMsgSent;
					} else {
						$lastAction = $lastLoginTime;
					}
					//calculation of expected answering datetime
					$responseDatetimeExpected = date('H:i d.m', strtotime($lastAction + $responseTimeAvg));

					return $responseDatetimeExpected;
				}
			}

			return 0;
		}

	}