<?php

class Message extends CActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }



	public function tableName()
	{
		return 'sy_message';
    }



	public function rules()
	{
		return array(
	        array('text, fromuser, touser, date', 'required'),
	        array('fromuser, touser', 'numerical', 'integerOnly' => true),
            array('endtime', 'safe'),
            array('id, text, fromuser, touser, date', 'safe', 'on' => 'search'),
        );
    }



	public function relations()
	{
		return array(
	        'relFromuser' => array(
		        self::BELONGS_TO,
		        'User',
		        'fromuser'
	        ),
	        'relTouser'   => array(
		        self::BELONGS_TO,
		        'User',
		        'touser'
	        ),
        );
    }



	public function attributeLabels()
	{
		return array(
            'id' => 'ID',
            'text' => 'Nachricht',
            'fromuser' => 'Von',
            'touser' => 'An',
            'date'   => 'Um',
        );
    }



	public static function getWordCount($text, $pure = false)
	{
		$wordCount = str_word_count($text);
		if ($pure) {
			return $wordCount;

		} else {
			if ($wordCount > 1) {
				return $wordCount . " Wörter";
			} else {
				return $wordCount . " Wort";
			}
		}
	}
}