<?php
	$this->pageTitle = "Neue Nachricht";
?>


<!-- Iteration durch alle Notizen und Anzeige eines Divs für jede Notiz-->
<div class="row">
	<?php foreach ($mUsers as $user): ?>
		<div class="col col-xs-12 col-lg-3" data-toggle="tooltip" data-placement="bottom" title="<?= $user->name ?> schreiben">
			<div class="well text-center grow-rotate">
				<h4>
					<?= $user->name; ?>
				</h4>
				<br>
				<a href="<?= Yii::app()->createUrl('message/view', array('idFromUser' => $user->id)) ?>"> <img src="<?= User::loadPictureUrl($user->picture) ?>"
				                                                                                               class="img-responsive img-rounded center-block" alt=""></a>
				<br>
				<small>
					<?= User::getWordCount($user->id) ?>
				</small>
			</div>
		</div>
	<?php endforeach; ?>
</div>