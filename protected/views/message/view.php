<script src="/js/jBeep/jBeep.min.js"></script>


<?php
	Yii::app()->clientScript->registerScript('message-update', '
		if(typeof(EventSource) !== "undefined") {
		    var source = new EventSource("' . CController::createUrl('message/getMsg', array('idFromUser' => $idFromUser)) . '");
		    source.onmessage = function(event) {
		        $("#newMessages").append(event.data).fadeIn(); // We want to display new messages above the stack
		        toastr.info("Neue Nachricht");
		        jBeep();
		    };
		}', CClientScript::POS_READY);
?>

<?php $this->pageTitle = User::getUsername($idFromUser); ?>

<?php //Benachrichtigungen
	$this->widget('ext.toastr.HzlToastr', array(
		'flashMessagesOnly' => true,
		'options'           => array(
			'timeOut' => 8000,
		)
	));
?>

<!-- Iteration durch alle Notizen und Anzeige eines Divs für jede Notiz-->
<?php foreach ($mMessages as $message): ?>
	<?php if ($message->fromuser == Yii::app()->user->id): ?>
		<!-- Nachricht von eingeloggtem Nutzer-->
		<div class="row">
			<div class="col col-lg-7  col-lg-offset-4 col-md-6 col-md-offset-4 col-xs-8">
				<div class="bubbleRight round-corners col-lg-12  col-lg-offset-0">
					<div class="row">
						<p class="col-lg-7 col-md-6 lead">
							<?= $message->relFromuser->name; ?>
						</p>
						<i class="text-right">
							<small class="text-muted"><?= Allgemein::doDateFormat($message->date) . ' ' . Allgemein::doTimeFormat($message->date); ?>
								<br>
								<?= Readstatus::getReadstatusMsg($message->id); ?>
								<?= Message::getWordCount($message->text); ?>
							</small>
						</i>
					</div>
					<br>
					<?= $message->text ?>
					<small>
						<a href="<?= Yii::app()->createUrl('message/deleteMsg', array('idMsg' => $message->id)) ?>" class="glyphicon glyphicon-trash pull-right"></a>
					</small>
				</div>
			</div>
			<div class="col col-lg-1  col-md-2 col-xs-4">
				<img src="<?= User::loadPictureUrl($message->relFromuser->picture) ?>" class="img-responsive img-rounded center-block" alt=""></a>
				<br>
			</div>
		</div>

	<?php else: ?>
		<!-- Nachricht des Gesprächspartners -->
		<div class="row">
			<div class="col  col-lg-1  col-md-2 col-xs-4">
				<img src="<?= User::loadPictureUrl($message->relFromuser->picture) ?>" class="img-responsive img-rounded center-block" alt=""></a>
				<br>
			</div>
			<div class="col col-lg-7 col-md-6 col-xs-8">
				<div class="bubbleLeft round-corners row col-lg-12 col-lg-offset-0">
					<div class="row">
						<p class="lead col-lg-6">
							<?= $message->relFromuser->name; ?>
						</p>
						<h5 class="col-lg-6 text-right">
							<i>
								<small>
									<?= Allgemein::doDateFormat($message->date) . ' ' . Allgemein::doTimeFormat($message->date); ?>
									<br>
									<?= Message::getWordCount($message->text); ?>
								</small>
							</i>
						</h5>
					</div>

					<br>
					<?= $message->text; ?>
				</div>
			</div>
		</div>

	<?php endif; ?>

<?php endforeach; ?>

<div id="newMessages"></div>

<div class="clearfix"></div>

<div class="row">
	<div class="col col-lg-7  col-lg-offset-4 col-md-6 col-md-offset-4 col-xs-8">
		<div class="bubbleRight round-corners col-lg-12  col-lg-offset-0"
		">
		<?php
			$form = $this->beginWidget('bootstrap.widgets.TbActiveForm');?>
		<?php $this->widget('ext.yii-redactor.ERedactorWidget', array(
			'model'     => $mNewMessage,
			'attribute' => 'text',
			'options'   => array(
				'buttons'           => array(
					'formatting',
					'bold',
					'italic',
					'underline',
					'alignment',
					'horizontalrule',
					'unorderedlist',
					'outdent',
					'indent',
					'link'
				),
				//'wym'               => true,
				'focus'             => true,
				'lang'              => 'de',
				'toolbarFixed'      => true,
				//'toolbarFixedBox'   => true,
				'cleanup'           => true,
				'removeEmptyTags'   => true,
				'convertImageLinks' => true,
				'convertVideoLinks' => true,
				'tidyHtml'          => true,
				'observeLinks'      => true,
				'direction'         => 'ltr',
				'toolbar'           => 'object',
				'shortcuts'         => true,
				'autoresize'        => true,
			)
		));
		?>
		<?= TbHtml::hiddenField('idFromUser', $idFromUser) ?>
		<?=
			TbHtml::submitButton("Schreiben", array(
				'class' => 'btn btn-info text-center round-corners col-lg-12 col-xs-12',
			));?>
		<?php $this->endWidget(); ?>
	</div>
</div>
<div class="col col-lg-1 col-md-2 col-xs-4">
	<img src="<?= User::loadPictureUrl(User::model()->findByPk(Yii::app()->user->id)->picture) ?>" class="img-responsive img-rounded center-block" alt=""></a> <br>
</div>
</div>