<div class="col-lg-10 col-lg-offset-1">
	<?php
		$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
			'action' => Yii::app()->createUrl($this->route),
			'method' => 'get',
		));
	?>

	<div class="row">
		<?= $form->textFieldControlGroup($model, 'id'); ?>
	</div>

	<div class="row">
		<?=
			$form->textFieldControlGroup($model, 'name', array(
				'size'      => 45,
				'maxlength' => 45
			)); ?>
	</div>

	<div class="row">
		<?=
			$form->passwordFieldControlGroup($model, 'password', array(
				'size'      => 45,
				'maxlength' => 45
			)); ?>
	</div>

	<div class="row">
		<?= $form->textFieldControlGroup($model, 'lastLoginTime'); ?>
	</div>

	<div class="row">
		<?= $form->textFieldControlGroup($model, 'picture'); ?>
	</div>

	<div class="row buttons">
		<?=
			TbHtml::submitButton("Suchen", array(
				'class' => 'btn btn-sm btn-info col-lg-12 col-xs-12',
			)) ?>
	</div>

	<?php $this->endWidget(); ?>
</div><!-- search-form -->
<div class="clearfix"></div>