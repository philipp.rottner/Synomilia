<?php
	//Tabuntertitel
	$this->pageTitle = "Profil";
?>
<?php //Benachrichtigungen
	$this->widget('ext.toastr.HzlToastr', array(
		'flashMessagesOnly' => true,
		'options'           => array(
			'timeOut' => 8000,
		)
	));
?>

<div class="row">
	<br><br>

	<div class="col-lg-4 col-lg-offset-3 text-center">
		<div class="well wobble-horizontal">
			<div>
				<h3>Profil anlegen</h3>
				<br>
			</div>
			<?php
				$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
					'layout'               => TbHtml::FORM_LAYOUT_INLINE,
					'id'                   => 'user-form',
					'enableAjaxValidation' => false,
					'htmlOptions'          => array(
						'enctype' => 'multipart/form-data'
					)
				));
			?>

			<?=
				$form->textFieldControlGroup($model, 'name', array(
					'size'        => 45,
					'maxlength'   => 45,
					'placeholder' => 'Benutzername',
					'span'        => 12,
				)); ?> <br> <br>

			<?=
				$form->passwordFieldControlGroup($model, 'password', array(
					'size'        => 45,
					'maxlength'   => 45,
					'placeholder' => 'Passwort',
				)); ?> <br> <br>

			<?=
				$form->passwordFieldControlGroup($model, 'password_repeat', array(
					'size'        => 45,
					'maxlength'   => 45,
					'placeholder' => 'Passwort wiederholen',
				)); ?> <br> <br>

			<?= $form->fileFieldControlGroup($model, 'picture'); ?> <br> <br>

			<?=
				TbHtml::submitButton("Erstellen", array(
					'class' => 'btn btn-sm btn-info col-lg-12',
				)); ?>

			<?php $this->endWidget(); ?>
		</div>
	</div>
</div>
