<?php
	//Tabuntertitel
	$this->pageTitle = "Nutzerverwaltung";
	Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#user-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="well text-center">
	<h3> Account-Administration </h3>

	<p>
		<small>
			<i> Optional lässt sich mit Vergleichsoperatoren arbeiten (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
				oder <b>=</b>), welche dem Suchparameter vorangestellt sein müssen: </i>
			<?php echo CHtml::link('Erweiterte Suche', '#', array('class' => 'search-button')); ?>
		</small>
	</p>

	<div class="search-form text-center" style="display:none">
		<?php
			$this->renderPartial('_search', array(
				'model' => $model,
			));
		?>
	</div>

	<div class="text-left">
		<?php
			$this->widget('bootstrap.widgets.TbGridView', array(
				'type'         => 'striped',
				'summaryText'  => '',
				'id'           => 'user-grid',
				'dataProvider' => $model->search(),
				'filter'       => $model,
				'columns'      => array(
					'id',
					'name',
					'password',
					array(
						'name'  => 'lastLoginTime',
						'value' => 'date("d.m.Y H:i", strtotime($data->lastLoginTime))',
					),
					'picture',
				),
			));
		?>
	</div>



	<p>
		<?=
			TbHtml::linkButton('Account anlegen', array(
				'class' => 'btn btn-sm btn-info col-lg-12 col-xs-12',
				'url'   => Yii::app()->createUrl('user/create'),
			)) ?>
	</p>
</div>
