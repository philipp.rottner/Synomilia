<?php
	$this->pageTitle = "Mein Profil";
?>

<div class="col col-lg-4 col-lg-offset-4 text-center col-xs-12">
<div class="well wobble-horizontal">
		<h3>
			<?= $model->name; ?> <br>
		</h3>
		<br>
		<img src="<?= User::loadPictureUrl($model->picture) ?>" class="img-responsive img-rounded center-block" alt="Profilbild"></a>
		<br>
		<?php
			$this->widget('bootstrap.widgets.TbDetailView', array(
				'data'       => $model,
				'attributes' => array(
					array(
						'name'  => 'lastLoginTime',
						'value' => Allgemein::doDateFormat($model->lastLoginTime) . ' um ' . Allgemein::doTimeFormat($model->lastLoginTime),
					),
					array(
						'name'  => 'Geschrieben',
						'value' => User::getWordCount($model->id),
					)
				),
			));
		?>
		<?=
			TbHtml::linkButton("Profil aktualisieren", array(
				'class' => 'btn btn-sm btn-info col-lg-12 col-xs-12 round-corners',
				'url'   => array('user/update'),
			))?> <br> <br>
		<?=
			TbHtml::linkButton("Profil löschen", array(
				'class' => 'btn btn-sm btn-danger col-lg-12 col-xs-12 round-corners',
				'url'   => array('user/delete'),
			))?>
		<br>
	</div>
</div>