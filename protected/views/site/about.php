<!--fullPage https://github.com/alvarotrigo/fullPage.js -->
<link rel="stylesheet" type="text/css" href="/css/fullPage/jquery.fullPage.css"/>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>
<script type="text/javascript" src="/js/fullPage/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="/js/fullPage/jquery.fullPage.min.js"></script>
<script type="text/javascript" src="/js/fullPage/examples.js"></script>
<!-- drowDownEffects http://tympanus.net/codrops/2012/11/29/simple-effects-for-drop-down-lists/ -->
<link rel="stylesheet" type="text/css" href="/css/dropDownEffects/style2.css"/>
<script src="/js/dropDownEffects/modernizr.custom.63321.js"></script>
<script type="text/javascript" src="/js/dropDownEffects/jquery.dropdown.js"></script>
<!-- READY: GO! -->
<script type="text/javascript">
	$(document).ready(function () {
		$('#fullpage').fullpage({
			anchors: ['infos', 'challenge', 'roadmap'],
			slidesColor: ['#1bbc9b', '#4BBFC3', '#7BAABE'],
			navigation: true,
			navigationPosition: 'right',
			navigationTooltips: ['Infos', 'Challenge', 'Roadmap'],
			slidesNavigation: true,
			loopTop: true,
			loopBottom: true,
			easing: 'easeOutBack'
		});
		$('#cd-dropdown').dropdown({
			gutter: 5,
			delay: 100,
			random: true
		});
		/*
		 var visitMeOn = $('#cd-dropdown');
		 visitMeOn.change(function (e) {
		 console.log("Dropdown used");
		 })*/
	});
</script>
<?php
	$this->pageTitle = "Über Synomillia";
?>

<?php /*Yii::app()->clientScript->registerScript('visitMeOnChange', 'function visitMeOnChange(){
	var visitMeOnChange = document.getElementById("cd-dropdown");
	var index = visitMeOnChange.options[visitMeOnChange.selectedIndex].value;
	if (index == 1) {
		console.log(1);
	}
	else if (index == 2) {
		console.log(2);
	}
	else if (index == 3) {
		console.log(3);
	} else {
		console.log(4);
	};
}'); */
?>


<div id="fullpage" class="text-center">
	<div class="section " id="section0">
		<div class="slide" id="slide1">
			<div class="intro">
				<h2>
					<b>
						<i class="fa fa-comments-o"></i> SYNOMILLIA
					</b>
				</h2>

				<h2>
					<small class="text-info">
						Share good news with your dearest
					</small>
				</h2>
				<br>

				<h3>
					<p class="lead">
						Product of one person being productive for one day. <br><br>
						<!--Than come and see where you can find me on the left...-->
						Think this app is beautiful and simple too? <br>
						Then come and see where you can find me on the left...<br><br>
						Got questions concerning safety and privacy? <br>
						Turn right...
					</p>
				</h3>
			</div>
		</div>


		<div class="slide" id="slide2">
			<b>
				<h2>
					<i class="fa fa-shield"></i> SECURITY
				</h2>
			</b>
			If you find a security breach, <a href="mailto:notimportant4my@live.com"> mail me </a>. <br>
			Please don't report functional bugs, I will fix them as soon as possible! <br> <br>

			<b>
				<h2>
					<i class="fa fa-hdd-o"></i> DATA SAFETY
				</h2>
			</b>
			The database is only accessible from localhost, meaning, datasets are <br>
			save from third eyes, unless the hoster or your profile on Synomillia self is hacked. <br>
			You can delete your account on your personal profile page accessible <a href="<?= Yii::app()->createUrl('user/view') ?>"> here </a>. <br>
			This includes all messages you sent, all read-statusses, the profile <br>
			picture (unless you stuck with the sweet cat) and the login access. <br>
			Once deleted, the data cannot be restored, so be carefull! <br> <br>
			<b>
				<h2>
					<i class="fa fa-eye-slash"></i> PRIVACY<br>
				</h2>
			</b>
			Your data are <b>not</b> encrypted (except for the password). <br>
			Nevertheless there is nothing to worry: <br>
			Only the admin can access them - and I won't peak, promise!
		</div>


		<div class="slide" id="slide3">
			<h2>
				<b>
					<div id="findMe">
						<i class="fa fa-location-arrow"></i> FIND ME
					</div>
				</b>
			</h2>



			<!--<script>function jsFunction(){
				console.log("event fired");
				  var myselect = document.getElementById("cd-dropdown");
				  alert(myselect.options[myselect.selectedIndex].value);
				}
			</script>-->
			<?= TbHtml::beginFormTb(); ?>
			<select id="cd-dropdown" class="cd-select" onchange="console.log('test');">
				<option value="-1" selected>Visit me on</option>
				<option value="1" class="icon-google-plus">Google Plus</option>
				<option value="2" class="icon-facebook">Facebook</option>
				<option value="3" class="icon-github">GitHub</option>
			</select>
			<?= TbHtml::endForm(); ?>

			<p>
				<br><br><br><br>
			</p>
		</div>
	</div>



	<div class="section" id="section1">
		<div class="slide" id="slide1">
			<div class="intro">
				<h2>
					<b>
						<i class="fa fa-check-square-o"></i> The Challenge
					</b>
				</h2>

				<h2>
					<small class="text-info">
						The goal: hacking my own productivity
					</small>
				</h2>
				<br>

				<h3>
					<p class="lead">
						I asked myself:<br><br>
					</p>
				</h3>
				<h4>
					<p>
						<i>What would happen, if I would focus on <b>one</b> and <b>one</b> task only for 24 hours?<br> <br>
							What am I capable of regarding rapid web-application prototyping?<br> <br>
							What extra knowledge can I archieve?
						</i> <br><br>

					<div class="progress progress-striped col-lg-6 col-lg-offset-3">
						<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
							<span class="sr-only">80% Complete</span>
						</div>
					</div>
					<br>
					</p>
				</h4>
				<br>

				<h3>
					<p class="lead">
						You are using the answer right know. And I didn't spend a cent. <br>Eager to know, what ressources I used? Scroll sideways...
					</p>
				</h3>
			</div>
		</div>

		<div class="slide" id="slide2">
			<h2>
				<b>
					<i class="fa fa-code"></i> Ressources
				</b>
			</h2> <br><br>

			<p class="lead text-left col-lg-7 col-lg-offset-6">
				Hosting: <a href="http://www.uberspace.de" target="_blank"> Uberspace </a> <br>
				PHP-Framework: <a href="http://www.yiiframework.com" target="_blank"> Yii </a> <br>
				CSS-Framework: <a href="http://www.getbootstrap.com" target="_blank"> Bootstrap </a> <br>
				Effects: <a href="http://ianlunn.github.io/Hover" target="_blank"> Hover.css </a> & <a href="https://github.com/alvarotrigo/fullPage.js"
				                                                                                       target="_blank"> FullPage.js </a> & others
			</p>

			<p class="lead text-left col-lg-9 col-lg-offset-3">
				<br>
				And of course my progress <br>
				was dependent on <br>
				<i class="fa fa-coffee"></i> + <i class="fa fa-cutlery"></i> + <i class="fa fa-heart"></i> + <i class="fa fa-smile-o"></i> + <i class="fa fa-music"> </i>
			</p>

			<p class="lead text-left col-lg-7 col-lg-offset-6">
				<br>
				Without the following I would <br>
				not have come far neiter <br>
				<i class="fa fa-globe"></i> + <i class="fa fa-apple"></i> + <i class="fa fa-html5"></i> + <i class="fa fa-stack-overflow"></i> + <i
					class="fa fa-dropbox"> </i> + <i class="fa fa-github"></i> <br><br>
			</p>
		</div>

	</div>



	<div class="section" id="section2">
		<div class="intro">
			<h2>
				<b>
					<i class="fa fa-road"> +</i> <i class="fa fa-map-marker"> =</i> The Roadmap
				</b>
			</h2> <br>

			<p class="lead text-left col-lg-8 col-lg-offset-4">
				Fix some bugs <br>
				Live Notifications <br>
				Near-real-time chatting <br>
				Group conversations <br>
				Search in messages <br>
				Update to Yii2-Framework <br>
				<b>Plenty to do - stay tuned!</b>
			</p>
		</div>
	</div>
</div>