<?php
	//Tabuntertitel
	$this->pageTitle = "Login";
?>
<?php //Benachrichtigungen
	$this->widget('ext.toastr.HzlToastr', array(
		'flashMessagesOnly' => true,
		'options'           => array(
			'timeOut' => 8000,
		)
	));
?>

<div class="row">
	<br><br>

	<div class="col-lg-4 col-lg-offset-4 text-center">
		<div class="well wobble-horizontal">
			<div>
				<h3><i class="fa fa-comments-o"> Synomillia </i></h3>
				<br>
			</div>
			<?php
				$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
					'id'                     => 'login-form',
					'layout'                 => TbHtml::FORM_LAYOUT_INLINE,
					'enableClientValidation' => true,
					'clientOptions'          => array(
						'validateOnSubmit' => true,
					),
				));
			?>
			<?=
				$form->textFieldControlGroup($model, 'username', array(
					'placeholder' => 'Benutzername',
				)); ?> <br> <br>
			<?=
				$form->passwordFieldControlGroup($model, 'password', array(
					'placeholder' => 'Passwort',
				)); ?> <br> <br>
			<?=
				TbHtml::submitButton("Login", array(
					'class' => 'btn btn-sm btn-info col-lg-12 col-xs-12 round-corners',
				)); ?>
			<?php $this->endWidget(); ?>
		</div>

		<br>

		&copy; <?php echo date('Y'); ?> Philipp Rottner
	</div>
</div>
