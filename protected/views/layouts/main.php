<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
	<meta charset="UTF-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="language" content="de"/>
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
	<title> <?php echo CHtml::encode($this->pageTitle); ?> </title>
	<!-- Bootstrap 3 -->
	<link rel="stylesheet" type="text/css" href="/protected/extensions/yiistrap-bs3/assets/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="/protected/extensions/yiistrap-bs3/assets/css/bootstrap-theme.min.css"/>
	<!-- Google Nexus Menu: http://tympanus.net/codrops/2013/07/30/google-nexus-website-menu/ -->
	<link rel="stylesheet" type="text/css" href="/css/googleNexusWebsiteMenu/component.css"/>
	<link rel="stylesheet" type="text/css" href="/css/googleNexusWebsiteMenu/normalize.css"/>
	<script src="/js/googleNexusWebsiteMenu/modernizr.custom.js"></script>
	<!-- Hover:  http://ianlunn.github.io/Hover/ -->
	<link rel="stylesheet" type="text/css" href="/css/hover/hover-min.css"/>
	<!-- Custom CSS -->
	<link rel="stylesheet" type="text/css" href="/css/custom.css"/>
</head>

<body>
<div class="container-fluid" id="page">
	<!-- Navbar + Nexus Menu -->
	<nav class="ztop navbar navbar-default" role="navigation">
		<ul id="gn-menu" class="gn-menu-main">
			<li class="gn-trigger">
				<a class="gn-icon fa-bars"></a>
				<nav class="gn-menu-wrapper">
					<div class="gn-scroller">
						<ul class="gn-menu">
							<?= Allgemein::getMenu(); ?>
						</ul>
					</div>
				</nav>
			</li>
			<li>
				<?php if (Allgemein::getCountUnread() > 0): ?>
					<p class="lead setdown"><?= $this->pageTitle; ?>&nbsp; <span class="label label-info"><?= Allgemein::getCountUnread() ?></span></p>
				<?php endif; ?>
				<?php if (Allgemein::getCountUnread() == 0): ?>
					<p class="lead setdown"><?= $this->pageTitle; ?></p>
				<?php endif; ?>
			</li>
			<li><a href="<?= Yii::app()->createUrl('site/logout') ?>"><i class="fa fa-sign-out"></i></a></li>
		</ul>
	</nav>
	<!-- Content -->
	<div id="pageContent">
		<?php echo $content; ?>
	</div>
</body>
<!-- Nexus Menu Scripts -->
<script src="/js/googleNexusWebsiteMenu/classie.js"></script>
<script src="/js/googleNexusWebsiteMenu/gnmenu.js"></script>
<script>
	new gnMenu(document.getElementById('gn-menu'));
</script>
</html>
