<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
	<meta charset="UTF-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="language" content="de"/>
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
	<title> <?php echo CHtml::encode($this->pageTitle); ?> </title>
	<!-- Bootstrap 3 -->
	<link rel="stylesheet" type="text/css" href="/protected/extensions/yiistrap-bs3/assets/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="/protected/extensions/yiistrap-bs3/assets/css/bootstrap-theme.min.css"/>
	<!-- Hover:  http://ianlunn.github.io/Hover/ -->
	<link rel="stylesheet" type="text/css" href="/css/hover/hover-min.css"/>
	<!-- Custom CSS -->
	<link rel="stylesheet" type="text/css" href="/css/custom.css"/>
	<link rel="stylesheet" type="text/css" href="/css/borderMenu/style5.css"/>
</head>

<body>
<div class="container">
	<div id="page">
		<?php echo $content; ?>
	</div>
</div>
</body>
</html>
