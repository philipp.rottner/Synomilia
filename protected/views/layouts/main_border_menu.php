<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
	<meta charset="UTF-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="language" content="de"/>
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
	<title> <?php echo CHtml::encode($this->pageTitle); ?> </title>
	<!-- Bootstrap 3 -->
	<link rel="stylesheet" type="text/css" href="/protected/extensions/yiistrap-bs3/assets/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="/protected/extensions/yiistrap-bs3/assets/css/bootstrap-theme.min.css"/>
	<!-- borderMenu: http://tympanus.net/codrops/2013/09/30/animated-border-menus/ -->
	<link rel="stylesheet" type="text/css" href="/css/borderMenu/icons.css"/>
	<link rel="stylesheet" type="text/css" href="/css/borderMenu/normalize.css"/>
	<link rel="stylesheet" type="text/css" href="/css/borderMenu/style5.css"/>
	<script src="/js/borderMenu/modernizr.custom.js"></script>
	<!-- Hover:  http://ianlunn.github.io/Hover/ -->
	<link rel="stylesheet" type="text/css" href="/css/hover/hover-min.css"/>
	<!-- Custom CSS -->
	<link rel="stylesheet" type="text/css" href="/css/custom.css"/>
</head>

<body>
<div class="container">
	<div class="col-xs-3 col-lg-2">
		<nav id="bt-menu" class="bt-menu">
			<a href="#" class="bt-menu-trigger push"><span>Menu</span></a>
			<ul>
				<?= Allgemein::getBorderMenu() ?>
			</ul>
			<ul>
				<li class="buzz-out"><a href="https://plus.google.com/u/0/+PhilippRottner" class="bt-icon icon-gplus" target="_blank">Google+</a></li>
				<li class="buzz-out"><a href="https://www.facebook.com/prottner" class="bt-icon icon-facebook" target="_blank">Facebook</a></li>
				<li class="buzz-out"><a href="https://github.com/j8lbr8ck" class="bt-icon icon-github" target="_blank">Github</a></li>
			</ul>
		</nav>
	</div>
	<div class="col-xs-9 col-lg-10" id="page">
		<?php echo $content; ?>
	</div>
</body>
<script src="/js/borderMenu/borderMenu.js"></script>
<script src="/js/borderMenu/classie.js"></script>
</html>
