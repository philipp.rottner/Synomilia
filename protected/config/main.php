<?php
	return array(
		'name'              => 'Synomillia',
		'basePath'          => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
		'sourceLanguage'    => 'de_de',
		'language'          => 'de_de',
		'defaultController' => 'Message',
		'aliases'           => array(
			Yii::setPathOfAlias('bootstrap', dirname(__FILE__) . '/../extensions/yiistrap-bs3'),
		),
		'import'            => array(
			'application.models.*',
			'application.components.*',
			//for registering yiistrap
			'bootstrap.helpers.TbHtml',
			'bootstrap.helpers.TbArray',
			'bootstrap.behaviors.TbWidget',
			'bootstrap.components.TbApi',
			'bootstrap.widgets.*',
		),
		'preload'           => array(
			'efontawesome',
		),
		'modules'           => array(),
		'components'        => array(
			//bootstrap (yiistrap) configuration
			'bootstrap'    => array(
				'class' => 'bootstrap.components.TbApi',
			),
			//fontaswesome configuration
			'efontawesome' => array(
				'class' => 'ext.EFontAwesome.components.EFontAwesome',
			),
			'user'         => array(
				'loginUrl'       => array('site/login'),
				'allowAutoLogin' => true,
			),
			'session'      => array(
				//Cookie-Authentifizierung ist nicht zwingend, aber erwünscht
				'cookieMode' => 'allow',
				//Session wird nach einem Tag beendet
				'timeout'    => 86400,
			),
			'urlManager'   => array(
				'urlFormat'      => 'path',
				'showScriptName' => false,
				'caseSensitive'  => false,
			),
			'db'           => array(
				'connectionString' => 'mysql:host=localhost; dbname=chat',
				'emulatePrepare'   => true,
				'username'         => 'chat',
				'password'         => 'AdFiaHikwiwiaHygsEun',
			),
			'errorHandler' => array(
				'errorAction' => 'site/error',
			),
			'log'          => array(
				'class'  => 'CLogRouter',
				'routes' => array(
					array(
						'class'  => 'CFileLogRoute',
						'levels' => 'error, warning',
					),
					// uncomment the following to show log messages on web pages
					array(
						'class' => 'CWebLogRoute',
					),
				),
			),
			//Registrierung der toastr-Benachrichtigungen ohne das PHP-Extension-Kompatibilitäts-Layer
			//Zum Verwenden des Aufrufs mittels "toastr.info(...)" o.ä. "Yii::app()->clientScript->registerPackage('toastr')" einbinden
			'clientScript' => array(
				'packages' => array(
					'toastr' => array(
						'basePath' => 'ext.toastr.assets',
						'js'       => array('toastr.min.js'),
						'css'      => array('toastr.min.css'),
					),
				),
			),
		),
		// application-level parameters that can be accessed
		// using Yii::app()->params['paramName']
		'params'            => array(),
	);
