<?php


	class MessageController extends Controller
	{

		public $layout = '//layouts/mainContent';
		public $defaultAction = 'view';



		/**
		 * @return array action filters
		 */
		public function filters()
		{
			return array(
				'accessControl',
				// perform access control for CRUD operations
				'postOnly + delete',
				// we only allow deletion via POST request
			);
		}



		/**
		 * Specifies the access control rules.
		 * This method is used by the 'accessControl' filter.
		 * @return array access control rules
		 */
		public function accessRules()
		{
			return array(
				array(
					'allow',
					// allow authenticated user to perform all actions
					'actions' => array(
						'view',
						'new',
						'sendMsg',
						'editMsg',
						'deleteMsg',
						'getMsg',
					),
					'users'   => array('@'),
				),
				array(
					'deny',
					// deny all users
					'users' => array('*'),
				),
			);
		}


		//
		//  Views message-timeline of current user with the specified or otherwise the last one
		//
		public function actionView($idFromUser = null)
		{
			$mNewMessage = new Message();
			if (isset($_POST['Message']['text'])) {
				$datetime = new \DateTime();
				$touser = $_POST['idFromUser'];
				$fromuser = Yii::app()->user->id;
				$text = $_POST['Message']['text'];
				$now = $datetime->format('Y-m-d H:i:s');
				$today = $datetime->format('Y-m-d') . "%";
				$yesterday = date("Y-m-d", strtotime('-1 days')) . "%";
				$randomSliceStart = 0;
				$randomSliceEnd = 0;
				while ($randomSliceEnd - $randomSliceStart <= strlen($text) / 50) {
					$randomSliceStart = rand(2, strlen($text));
					$randomSliceEnd = rand($randomSliceStart, strlen($text));
				}
				//echo $text;
				$randomDuplicateCheck = "%" . substr($text, $randomSliceStart, $randomSliceEnd) . "%";
				//echo $randomDuplicateCheck;

				$critExistsMessage = new CDbCriteria();
				$critExistsMessage->addCondition("fromuser = '$fromuser'");
				$critExistsMessage->addCondition("touser = '$touser'");
				$critExistsMessage->addCondition("text LIKE '$randomDuplicateCheck'");
				$critExistsMessage->addCondition("date LIKE '$today' OR date LIKE '$yesterday'");
				$mExistsMessage = Message::model()->find($critExistsMessage);
				//var_dump($mExistsMessage);
				//exit();
				if (empty($mExistsMessage)) {
					$mNewMessage->text = $text;
					$mNewMessage->fromuser = $fromuser;
					$mNewMessage->touser = $touser;
					$mNewMessage->date = $now;
					if ($mNewMessage->save()) {
						Yii::app()->user->setFlash('Erfolg', 'Nachricht erfolgreich gesendet');
						$mNewMessage = new Message();
					} else {
						Yii::app()->user->setFlash('Fehler', 'Senden der Nachricht gescheitert. Bitter erneut versuchen');
					}
				} else {
					$mNewMessage = new Message();
				}
			}

			$idToUser = Yii::app()->user->id;
			//Kein Chatpartner übergeben --> Letzter Chatparter wird geladen
			if (is_null($idFromUser)) {
				$critLastMessageToUser = new CDbCriteria();
				$critLastMessageToUser->addCondition("touser = '$idToUser'");
				$critLastMessageToUser->order = 'date DESC';
				$mLastMessageToUser = Message::model()->findAll($critLastMessageToUser);
				if (!empty($mLastMessageToUser)) {
					$lastMessage = $mLastMessageToUser[0];
					$idFromUser = $lastMessage->fromuser;
					//$this->refresh($idFromUser);
				} //Nutzer hat noch keinen Chat begonnen --> Weiterleitung zur Kontaktliste
				else {
					$this->redirect('message/new');
				}
			}
			$critMessage = new CDbCriteria();
			$critMessage->addCondition("touser = '$idToUser' or fromuser = '$idToUser'");
			$critMessage->addCondition("touser = '$idFromUser' or fromuser = '$idFromUser'");
			$critMessage->order = 'date ASC';
			$mMessages = Message::model()->findAll($critMessage);
			$this->render('view', array(
				'mMessages'   => $mMessages,
				'idFromUser'  => $idFromUser,
				'mNewMessage' => $mNewMessage,
			));
			Readstatus::setReadstatusChat($idFromUser);
		}


		//
		//  Selection to start a new message
		//
		public function actionNew()
		{
			$idUser = Yii::app()->user->id;
			$critUser = new CDbCriteria();
			$critUser->addCondition("id != '$idUser'");
			$critUser->order = 'name ASC';
			$mUsers = User::model()->findAll($critUser);
			$this->render('new', array(
				'mUsers' => $mUsers,
			));
		}



		//
		//  Deletes message
		//
		public function actionDeleteMsg($idMsg)
		{
			$mMessage = Message::model()->findByPk($idMsg);
			if (!empty($mMessage)) {
				if ($mMessage->fromuser == Yii::app()->user->id) {
					$idFromUser = $mMessage->touser;
					if ($mMessage->delete()) {
						Yii::app()->user->setFlash('Erfolg', 'Nachricht erfolgreich gelöscht');
					} else {
						Yii::app()->user->setFlash('Fehler', 'Löschen der Nachricht gescheitert. Bitter erneut versuchen');
					}
					$this->redirect(Yii::app()->createUrl('message/view', array('idFromUser' => $idFromUser)));
				} else {
					Yii::app()->user->setFlash('Warnung', 'Wer versucht, zu hacken, fliegt heraus!');
					$this->redirect(array('site/logout'));
				}
			} else {
				Yii::app()->user->setFlash('Warnung', 'Wer versucht, zu hacken, fliegt heraus!');
				$this->redirect(array('site/logout'));
			}
		}





		//
		//  Returns new messages checked regulary in a fixed time-intervall
		//
		public function actionGetMsg($idFromUser)
		{
			//GET MESSAGES OF CHAT-PARTNER WHERE NO READ STATUS EXISTS AND SET THEM READ
			$idTouser = Yii::app()->user->id;
			$critMessagesUnread = new CDbCriteria();
			$critMessagesUnread->addCondition("fromuser = '$idFromUser'");
			$critMessagesUnread->addCondition("touser = '$idTouser'");
			//$critMessagesUnread->join = 'JOIN sy_readstatus ON t.id = sy_readstatus.message';
			//$critMessagesUnread->addCondition("sy_readstatus.messageaa IS NULL");
			$critMessagesUnread->addCondition('id NOT IN (SELECT message FROM sy_readstatus)');
			$mMessagesUnread = Message::model()->findAll($critMessagesUnread);

			header('Content-Type: text/event-stream');
			header('Cache-Control: no-cache');
			echo "retry: 60000\n\n"; // Optional. We tell the browser to retry after 10 seconds

			if (!empty($mMessagesUnread)) {
				foreach ($mMessagesUnread as $msg) {
					echo "data: <div class='row'><div class='col col-lg-1 col-md-2 col-xs-4'><img src='" . User::loadPictureUrl($msg->relFromuser->picture) . "' class='img-responsive img-rounded center-block' alt=''></a><br></div><div class='col col-lg-7 col-md-6 col-xs-8'><div class='bubbleLeft round-corners row col-lg-12 col-lg-offset-0'><div class='row'><p class='lead col-lg-6'>" . $msg->relFromuser->name . "</p><h5 class='col-lg-6 text-right'><i><small>" . Allgemein::doDateFormat($msg->date) . " " . Allgemein::doTimeFormat($msg->date) . "<br>" . Message::getWordCount($msg->text) . "</small></i></h5></div><br>" . $msg->text . "</div></div></div>\n\n";
				}
				Readstatus::setReadstatusChat($idFromUser);
			}

			flush();
		}
	}