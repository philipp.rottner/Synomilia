<?php

	class UserController extends Controller
	{

		public $layout = '//layouts/mainContent';



		public function filters()
		{
			return array(
				'accessControl',
				// perform access control for CRUD operations
				'postOnly + delete',
				// we only allow deletion via POST request
			);
		}



		public function accessRules()
		{
			return array(
				array(
					'allow',
					// allow authenticated user to perform 'view', 'update' and 'delete' actions
					'actions' => array(
						'view',
						'update',
					),
					'users'   => array('@'),
				),
				array(
					'allow',
					// allow admin user to perform 'admin' action
					'actions' => array(
						'admin',
						'create',
						'delete',
					),
					'users'   => array('philipp'),
				),
				array(
					'deny',
					// deny all users
					'users' => array('*'),
				),
			);
		}




		//
		//  Views current user
		//
		public function actionView()
		{
			$id = Yii::app()->user->id;
			$this->render('view', array(
				'model' => $this->loadModel($id),
			));
		}



		//
		// Creates a new user
		//
		public function actionCreate()
		{
			$model = new User;
			if (isset($_POST['User'])) {
				$model->attributes = $_POST['User'];
				if (!empty($_FILES['User']['tmp_name']['picture'])) {
					$rnd = rand(0, 999999); // generate random number between 0-9999
					$file = CUploadedFile::getInstance($model, 'picture');
					$fileName = substr("{$rnd}-{$file}", -100);
					$model->picture = $fileName;
					if ($model->save()) {
						$file->saveAs('pictures/' . $fileName);
						$this->redirect(array('user/view'));
					}
				} else {
					$model->picture = "";
					if ($model->save()) {
						$this->redirect(array('user/view'));
					}
				}
			}
			$this->render('create', array(
				'model' => $model,
			));
		}




		//
		//  Updates current user
		//
		public function actionUpdate()
		{
			$model = $this->loadModel(Yii::app()->user->id);
			// Uncomment the following line if AJAX validation is needed
			// $this->performAjaxValidation($model);
			if (isset($_POST['User'])) {
				$model->attributes = $_POST['User'];
				if (!empty($_FILES['User']['tmp_name']['picture'])) {
					//TODO: DELETE OLD PICTURE
					$rnd = rand(0, 999999); // generate random number between 0-9999
					$file = CUploadedFile::getInstance($model, 'picture');
					$fileName = substr("{$rnd}-{$file}", -100);
					$model->picture = $fileName;
					if ($model->save()) {
						$file->saveAs('pictures/' . $fileName);
						$this->redirect(array('user/view'));
					}
				} else {
					//TODO: DELETE OLD PICTURE
					$model->picture = "";
					if ($model->save()) {
						$this->redirect(array('user/view'));
					}
				}
			}
			$this->render('update', array(
				'model' => $model,
			));
		}



		//
		//  Delete current user and all messages send by him
		//
		public function actionDelete()
		{
			$idUser = Yii::app()->user->id;
			if ($this->loadModel($idUser)->delete()) {
				Yii::app()->user - setFlash('Erfolg', 'Dein Nutzeraccount wurde erfolgreich gelöscht');
				$critMessage = new CDbCriteria();
				$critMessage->addCondition("fromuser = '$idUser'");
				$mMessages = Message::model()->findAll($critMessage);
				foreach ($mMessages as $message) {
					$message->delete();
				}
				$critReadstatus = new CDbCriteria();
				$critReadstatus->addCondition("user = '$idUser'");
				$mReadstatus = Readstatus::model()->findAll($critReadstatus);
				foreach ($mReadstatus as $readstatus) {
					$readstatus->delete();
				}
				//TODO: DELETE PICTURE TOO
			} else {
				Yii::app()->user - setFlash('Fehler', 'Dein Nutzeraccount wurde nicht gelöscht');
			}
			$this->redirectarray('site/login');
		}



		public function actionAdmin()
		{
			$model = new User('search');
			$model->unsetAttributes(); // clear any default values
			if (isset($_GET['User'])) {
				$model->attributes = $_GET['User'];
			}

			$this->render('admin', array(
				'model' => $model,
			));
		}



		public function loadModel($id)
		{
			$model = User::model()->findByPk($id);
			if ($model === null) {
				throw new CHttpException(404, 'The requested page does not exist.');
			}

			return $model;
		}



		protected function performAjaxValidation($model)
		{
			if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-form') {
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}
		}

	}